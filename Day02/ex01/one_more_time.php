#!/usr/bin/php
<?php
/********** CHECK STRING FORMAT **********/
function is_valid_format($tab)
{
	return (preg_match("/(^[Ll]undi|^[Mm]ardi|^[Mm]ercredi|^[Jj]eudi|^[Vv]endredi|^[Ss]amedi|^[Dd]imanche) (0[1-9]|[1-2][0-9]|3[0-1]) ([Jj]anvier|[Ff]evrier|[Ff]évrier|[Mm]ars|[Aa]vril|[Mm]ai|[Jj]uin|[Jj]uillet|[Aa]out|[Aa]oût|[Ss]eptembre|[Oo]ctobre|[Nn]ovembre|[Dd]ecembre|[Dd]écembre) (\d){4} ([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/",$tab)) ? TRUE : FALSE;
}
/********** CHECK IF DAYS CORRESPOND TO MONTH **********/
function is_valid_day($tab) {
	return (checkdate($tab[2], $tab[1], $tab[3])) ? TRUE : FALSE;
}
/********** CONVERT MONTH INTO NUM VALUE **********/
function convert_month($tab) {
	$month = ucfirst($tab);
	switch ($month) {
		case "Janvier":
			$value = 1;
			break;
		case "Fevrier":
			$value = 2;
			break;
		case "Mars":
			$value = 3;
			break;
		case "Avril":
			$value = 4;
			break;
		case "Mai":
			$value = 5;
			break;
		case "Juin":
			$value = 6;
			break;
		case "Juillet":
			$value = 7;
			break;
		case "Aout":
			$value = 8;
			break;
		case "Septembre":
			$value = 9;
			break;
		case "Octobre":
			$value = 10;
			break;
		case "Novembre":
			$value = 11;
			break;
		case "Decembre":
			$value = 12;
			break;
		case "Février":
			$value = 2;
			break;
		case "Août":
			$value = 8;
			break;
		case "Décembre":
			$value = 12;
			break;
	}
	return $value;
}

if ($argc == 2)
{
	date_default_timezone_set("Europe/Paris");

	if (is_valid_format($argv[1]) == true) {
		$str = str_replace(":", " ", $argv[1]);
		$t = explode(" ", $str);
		$t[2] = convert_month($t[2]);
		$ret = is_valid_day($t);
	}
	if ($ret == TRUE)
	{
		$var = mktime($t[4], $t[5], $t[6], $t[2], $t[1], $t[3]);
		echo "$var\n";
	} else {
		echo "Wrong Format\n";
		exit(0);
	}
}

 ?>
