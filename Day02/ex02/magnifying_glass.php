#!/usr/bin/php
<?php

function replace_occurence($tab)
{
  return str_replace($tab[1], strtoupper($tab[1]),$tab[0]);
}

if ($argc == 2)
{
  $str = "";
  $file = fopen($argv[1], "r");
  $link = "/<a.*?>(.*?)</";
  $title = '/<a.*?title="(.*?)">/';
  while (!feof($file))
  {
    $str .= fgets($file);
  }
  fclose($file);
  $str = preg_replace_callback($title, "replace_occurence", $str);
  $str = preg_replace_callback($link, "replace_occurence", $str);
}
echo $str;
?>