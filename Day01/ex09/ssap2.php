#!/usr/bin/php
<?php

function ft_split($tab) {
	$split = array_filter(explode(" ", $tab));
	return $split;
}

function ft_cmp($s1, $s2) {
	$s1 = strtolower($s1);
	$s2 = strtolower($s2);

	$i = 0;
	while ($i < strlen($s1) && $i < strlen($s2) && $s1[$i] == $s2[$i])
		$i++;
	if ($i == strlen($s1) && $i != strlen($s2))
		return -1;
	elseif ($i == strlen($s2) && $i != strlen($s1))
		return 1;
	if (ereg('[a-z]', $s1[$i])) {
		if (ereg('[a-z]', $s2[$i]))
			return ($s1[$i] < $s2[$i]) ? -1 : 1;
		return -1;
	}elseif (ereg('[0-9]', $s1[$i])) {
		if (ereg('[0-9]', $s2[$i]))
			return ($s1[$i] < $s2[$i]) ? -1 : 1;
		elseif (ereg('[a-z]', $s2[$i]))
			return 1;
		return -1;
	}else {
		if (ereg('[a-z0-9]', $s2[$i]))
			return 1;
		return ($s1[$i] < $s2[$i]) ? -1 : 1;
	}
}

$tab = [];
for ($i = 1; $i < $argc ; $i++) {
	$tmp = ft_split($argv[$i]);
	$tab= array_merge($tab, $tmp);
}

usort($tab, "ft_cmp");

foreach ($tab as $t) {
	echo "$t\n";
}
?>
