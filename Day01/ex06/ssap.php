#!/usr/bin/php
<?php

function ft_split($str) {
	$ar = array_filter(explode(' ', $str));
	return $ar;
}

$ar = [];

for ($i = 1; $i < $argc; $i++) {
	$tmp = ft_split($argv[$i]);
	$ar = array_merge($ar, $tmp);
}

sort($ar);

foreach ($ar as $a) {
	echo $a . "\n";
}
?>
