#!/usr/bin/php
<?php
$nb;
$loop = 1;
while ($loop) {
	echo "Enter a number : ";
	$nb = fgets(STDIN);
	if ($nb == "") {
		echo "^D\n";
		$loop = 0;
	} else {
		$nb = trim($nb);
		if (is_numeric($nb)) {
			if ($nb % 2 == 0)
				echo "The number $nb is even\n";
			else
				echo "The number $nb is odd\n";
		} else
			echo "'$nb' is not a number\n";
	}
}
?>
