#!/usr/bin/php
<?php

if ($argc == 4)
{
  $nb1 = trim($argv[1]);
  $op = trim($argv[2]);
  $nb2 = trim($argv[3]);
  $res;
  if (is_numeric($nb1) && is_numeric($nb2))
  {
    switch ($op) {
      case "+":
        $res = $nb1 + $nb2;
        break;
      case "-":
        $res = $nb1 - $nb2;
        break;
      case "*":
        $res = (int)$nb1 * (int)$nb2;
        break;
      case "/":
        $res = $nb1 / $nb2;
        break;
      case "%":
        $res = $nb1 % $nb2;
        break;
      default:
        echo "Incorrect Parameters\n";
        break;
    }
    echo $res . "\n";
  }
  else
    echo "Incorrect Parameters\n";
}
else
  echo "Incorrect Parameters\n";




 ?>
