#!/usr/bin/php
<?php
/********** COUNT WORD NUMBER **********/
function count_word($str) {
	$i = 0;
	$word = 0;
	while ($i < strlen($str)) {
		if (ereg("[a-zA-Z0-9]", $str[$i])) {
			$word++;
			$i++;
		} elseif (ereg("[%+-\*\/]", $str[$i])) {
			$word++;
			$i++;
		} else
			$i++;
	}
	return $word;
}
/********** CHECK STR FORMAT **********/
function check_format($var) {
	return (ereg("^[0-9]+[%+-\*\/][0-9]+$", $var)) ? TRUE : FALSE;
}

/********** DO CALCULATION **********/
function do_op($tab) {
	switch ($tab[2]) {
		case "+":
			$res = $tab[1] + $tab[3];
			break;
		case "-":
			$res = $tab[1] - $tab[3];
			break;
		case "*":
			$res = (int)$tab[1] * (int)$tab[3];
			break;
		case "/":
			$res = $tab[1] / $tab[3];
			break;
		case "%":
			$res = $tab[1] % $tab[3];
			break;
		default:
			echo "Incorrect Parameters\n";
			break;
	}
	return $res;
}

if ($argc == 2)
{
	$nb_word = count_word($argv[1]);
	if ($nb_word == 3) {
		$ret = str_replace(" ", "", $argv[1]);
		$val = check_format($ret);
		if ($val == TRUE) {
			ereg("([0-9]+)([%+-\*\/])([0-9]+)", $ret, $ar);
			echo do_op($ar) . "\n";
		} else
			echo "Syntax Error\n";
	} else
		echo "Syntax Error\n";
}
else
  echo "Incorrect Parameters\n";

?>
