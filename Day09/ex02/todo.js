let i = 0;
let cookies = {};

if (document.cookie && document.cookie != '') {
	let split = document.cookie.split(';');
	for (let i = 0; i < split.length; i++) {
		let name_value = split[i].split("=");

		let parentElement = document.getElementById("ft_list");
		let firstChild = parentElement.firstChild;

		let added = document.createElement("div");
		added.setAttribute("id", name_value[0].trim());
		added.setAttribute("onclick", "del('" + name_value[0].trim() + "');");
		let text = document.createTextNode(name_value[1]);
		added.appendChild(text);

		parentElement.insertBefore(added, firstChild);
		let number = name_value[0].split('o');
		let nbr = parseInt(number[2]);
	}
}

if (nbr > 0)
	i = nbr + 1;

function add() {
	if (todo = prompt("Que veux-tu ajouter?")) {
		let parentElement = document.getElementById("ft_list");
		let firstChild = parentElement.firstChild;

		let added = document.createElement("div");
		added.setAttribute("id", "todo" + i);
		added.setAttribute("onclick", "del('todo" + i + "');");
		let text = document.createTextNode(todo);
		added.appendChild(text);

		parentElement.insertBefore(added, firstChild);

		document.cookie = "todo" + i + "=" + todo + ";";

		i++;
	}
}

function del(id) {
	if (confirm("Es-tu sur de vouloir supprimer cette tâche !?")) {
		let list = document.getElementById("ft_list");
		let div = document.getElementById(id);
		list.removeChild(div);
		document.cookie = id + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
	}
}