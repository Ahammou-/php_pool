<?php
function check_login($auth, $tab) {
    $flag = FALSE;
    foreach ($tab as $k => $v) {
        if ($auth['login'] == $v['login'] && $auth['oldpw'] == $v['passwd'] && $auth['newpw'] != $v['passwd'])
           $flag = TRUE;
    }
    return $flag;
}

if ($_POST['submit'] === 'OK') {
    if ($_POST['login'] != '' && $_POST['oldpw'] != '' && $_POST['newpw'] != '') {
        $auth['login'] = $_POST['login'];
        $auth['oldpw'] = hash("whirlpool", $_POST['oldpw']);
        $auth['newpw'] = hash("whirlpool", $_POST['newpw']);
        if (file_exists("../private")) {
            $array = unserialize(file_get_contents("../private/passwd"));
            if (check_login($auth, $array)) {
                echo "OK\n";
                foreach ($array as $k => $v) {
                    if ($auth['login'] == $v['login']) {
                        $array[$k]['passwd'] = $auth['newpw'];
                    }
                }
                $srlz = serialize($array);
                file_put_contents("../private/passwd", $srlz);
            } else {
                echo "ERROR\n";
            }
        } else {
            echo "ERROR\n";
        }
    } else {
        echo "ERROR\n";
    }
} else {
    echo "ERROR\n";
}
?>