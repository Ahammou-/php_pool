<?php

function auth($login, $passwd)
{
    $flag = FALSE;
    $log = $login;
    $pwd = hash("whirlpool", $passwd);
    $data = file_get_contents("../private/passwd");
    $unser = unserialize($data);
    
    foreach ($unser as $k => $v) {
        if ($log == $v['login'] && $pwd == $v['passwd'])
           $flag = TRUE;
    }
    return $flag;
}