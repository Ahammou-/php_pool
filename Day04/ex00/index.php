<?php
session_start();
if ($_GET != NULL && $_GET["submit"] == "OK") {
    $_SESSION["login"] = $_GET["login"];
    $_SESSION["passwd"] = $_GET["passwd"];
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        * {
            box-sizing: border-box;
        }
        form {
            width: 400px;
            margin: auto;
            margin-top: 250px;
            border: 1px solid #ccc;
        }
        table, tr, td {
            width: 100%;
        }
        #send-btn
        {
            width: 100%;
        }
    </style>
</head>
<body>
    <form action="index.php" method="GET">
        <table>
            <tr>
                <td>
                  <label for="login">login :</label>
                </td>
                <td>
                 <input type="text" name="login" value="<?php echo $_SESSION["login"] ?>">
                </td>
            </tr>
            <tr>
                <td>
                    <label for="password">password :</label>
                </td>
                <td>
                    <input type="password" name="passwd" value="<?php echo $_SESSION["passwd"]?>">
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input id="send-btn" type="submit" name="submit" value="OK">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>