<?php
session_start();
date_default_timezone_set("Europe/Brussels");

$path = "../private/chat";
if ($_SESSION['logged_user'] && $_POST['submit'] == "OK") {
    $chat['login']= $_SESSION['logged_user'];
    $chat['time'] = time();
    $chat['msg'] = $_POST['msg'];

    if (!file_exists($path)) {
        $fd = fopen($path, "w");
        flock($fd, LOCK_EX);
        $tab[] = $chat;
        file_put_contents($path, serialize($tab));
        flock($fd, LOCK_UN);
        fclose($fd);
    }else {
        $tab = unserialize(file_get_contents($path));
        $tab[] = $chat;
        $fd = fopen($path, "w");
        flock($fd, LOCK_EX);
        file_put_contents($path, serialize($tab));
        flock($fd, LOCK_UN);
        fclose($fd);
    }
    echo "<head>";
    echo "<script langage='javascript'>top.frames['chat'].location = 'chat.php';</script>";
    echo "</head>";
    echo "<form action='speak.php' method='POST'>";
    echo "<input type='text' name='msg' autofocus>";
    echo "<input type='submit' name='submit' value='OK'>";
    echo "</form>\n";
}elseif (!$_POST) {
    echo "<head>";
    echo "<script langage='javascript'>top.frames['chat'].location = 'chat.php';</script>";
    echo "</head>";
    echo "<form action='speak.php' method='POST'>";
    echo "<input type='text' name='msg' autofocus>";
    echo "<input type='submit' name='submit' value='OK'>";
    echo "</form>\n";
}else
    echo "ERROR\n";

?>