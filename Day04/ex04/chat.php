<?php
session_start();
header("Refresh: 0");
date_default_timezone_set("Europe/Brussels");

$path = "../private/chat";
if (file_exists($path)) {
    $tab = unserialize(file_get_contents($path));
    
    foreach ($tab as $key => $val)
        echo "[".date("H:i", $val['time'])."] "."<b>".$val['login']."</b>: ".$val['msg']."<br />"."\n";
}

?>