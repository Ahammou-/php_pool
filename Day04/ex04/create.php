<?php

function check_login($login, $array) {
    foreach ($array as $key => $val)
        if ($login == $val['login'])
            return TRUE;

    return FALSE;
}

$path = "../private/passwd";
if ($_POST['submit'] === 'OK') {
    if ($_POST['login'] != '' && $_POST['passwd'] != '') {
        $new['login'] = $_POST['login'];
        $new['passwd'] = hash("whirlpool", $_POST['passwd']);

        if (file_exists($path)) {
            $array = unserialize(file_get_contents($path));

            if (check_login($new['login'], $array)) {
                echo "ERROR\n";
            }else {
                $array[] = $new;
                file_put_contents($path, serialize($array));
                header('Location: index.html');
                echo "OK\n";
            }
        }else {
            $array[] = $new;
            mkdir("../private");
            file_put_contents($path, serialize($array));
            header('Location: index.html');
            echo "OK\n";
        }
    }else
        echo "ERROR\n";
}else
    echo "ERROR\n";

?>