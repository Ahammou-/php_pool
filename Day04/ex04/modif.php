<?php

function check_login($user, $array) {
    foreach ($array as $key => $val)
        if ($user['login'] == $val['login'] && $user['oldpw'] == $val['passwd'] && $user['newpw'] != $val['passwd'])
            return TRUE;
    
    return FALSE;
}

$path = "../private/passwd";
if ($_POST['submit'] === 'OK') {
    if ($_POST['login'] != '' && $_POST['oldpw'] != '' && $_POST['newpw'] != '') {
        $user['login'] = $_POST['login'];
        $user['oldpw'] = hash("whirlpool", $_POST['oldpw']);
        $user['newpw'] = hash("whirlpool", $_POST['newpw']);

        if (file_exists($path)) {
            $array = unserialize(file_get_contents($path));

            if (check_login($user, $array)) {
                foreach ($array as $key => $val)
                    if ($user['login'] == $val['login'])
                        $array[$key]['passwd'] = $user['newpw'];
                file_put_contents($path, serialize($array));
                header('Location: index.html');
                echo "OK\n";
            }else
                echo "ERROR\n";
        }else
           echo "ERROR\n";
    }else
        echo "ERROR\n";
}else
    echo "ERROR\n";

?>