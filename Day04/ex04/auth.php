<?php

function auth($login, $passwd) {
    $path = "../private/passwd";

    if (file_exists($path)) {
        $passwd = hash("whirlpool", $passwd);
        $array = unserialize(file_get_contents($path));
        foreach ($array as $key => $val)
            if ($login == $val['login'] && $passwd == $val['passwd'])
                return TRUE;
    }else
        return FALSE;
}

?>