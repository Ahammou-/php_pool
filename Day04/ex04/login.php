<?php
session_start();
require("auth.php");

if (auth($_POST["login"], $_POST["passwd"]))
    $_SESSION["logged_user"] = $_POST["login"];
else
    $_SESSION["logged_user"] = "";
    
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <style>
        * {
            box-sizing: border-box;
        }
        body {background-color: #ccc;}
        .container {
            width: 500px;
            margin: 0 auto;
            border: 1px solid #ccc;
            padding: 20px;
            margin-top : 20px;
            border-radius:20px;
            background-color: #a65959;
            box-shadow: 8px 8px 8px #000;
        }
        h2 {
            text-align:center;
            color: #fff;
        }
        #chat {
            width: 100%;
            height: 550px;
            background-color: #fff;
        }
        #speak {
            width:100%;
            height: 50px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>19 CHAT</h2>
        <iframe id="chat" src="chat.php" frameborder="2"></iframe>
        <iframe id="speak" src="speak.php" frameborder="2"></iframe>
    </div>
</body>
</html>
