<?php
function check_login($login, $tab) {
    $flag = FALSE;
    foreach ($tab as $key => $value) {
        if ($login == $value['login'])
            $flag = TRUE;
    }
    return $flag;
}
if ($_POST['submit'] === 'OK') {
    if ($_POST['login'] != '' && $_POST['passwd'] != '') {
        $new['login'] = $_POST['login'];
        $new['passwd'] = hash("whirlpool", $_POST['passwd']);
        if (!file_exists("../private") || !file_exists("../private/passwd")) {
            echo "OK\n";
            $array[] = $new;
            mkdir("../private");
            $srlz = serialize($array);
            file_put_contents("../private/passwd", $srlz);
        } else {
            $array = unserialize(file_get_contents("../private/passwd"));
            if (check_login($new['login'], $array)) {
                echo "ERROR\n";
            } else {
                echo "OK\n";
                $array[] = $new;
                $srlz = serialize($array);
                file_put_contents("../private/passwd", $srlz);
            }
        }
    } else {
        echo "ERROR\n";
    }
}else {
    echo "ERROR\n";
}
?>