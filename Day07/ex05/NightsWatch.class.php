<?php 

class NightsWatch {

    private $_fighters = [];

    public function recruit($person) {
        if ($person instanceof IFighter)
            array_push($this->_fighters, $person);
        //var_dump($this->_fighters);
    }

    public function fight()
    {
        foreach ($this->_fighters as $fighter)
            echo $fighter->fight();
    }
}