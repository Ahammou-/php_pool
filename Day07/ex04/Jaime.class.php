<?

class Jaime extends Lannister {

    public function sleepWith($with)
    {
        $className = get_class($with);
        
        if ($className == 'Tyrion')
            echo "Not even if I'm drunk !" . PHP_EOL;
        if ($className == 'Sansa')
            echo "Let's do this." . PHP_EOL;
        if ($className == 'Cersei')
            echo "With pleasure, but only in a tower in Winterfell, then."  . PHP_EOL;
    }
}
