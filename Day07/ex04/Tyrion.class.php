<?php 

class Tyrion extends Lannister {

    public function sleepWith($with)
    {
        $className = get_class($with);
        
        if ($className == 'Jaime')
            echo "Not even if I'm drunk !" . PHP_EOL;
        if ($className == 'Sansa')
            echo "Let's do this." . PHP_EOL;
        if ($className == 'Cersei')
            echo "Not even if I'm drunk !" . PHP_EOL;
    }
}