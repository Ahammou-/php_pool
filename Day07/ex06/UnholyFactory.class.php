<?php

class UnholyFactory {
    private $_army;

    public function __construct() {
        $this->_army = [];
    }

    public function absorb($soldier) {
        if ($soldier instanceof Fighter) {
            $this->name = $soldier->__tostring();
            if(!in_array($soldier, $this->_army)) {
                echo "(Factory absorbed a fighter of type " . $this->name . ")\n";
                array_push($this->_army, $soldier);
            } else
                echo "(Factory already absorbed a fighter of type " . $this->name . ")\n";
        } else
            echo "(Factory can't absorb this, it's not a fighter)\n";
    }

    private static function _getClass($rf)
    {
        switch ($rf) {
            case 'foot soldier':
                return new Footsoldier();
            case 'archer':
                return new Archer();
            case 'assassin':
                return new Assassin();
            default:
                return NULL;
        }
    }

    public function fabricate($rf) {
        $this->name = $rf;

        if (in_array($rf, $this->_army))
            echo "(Factory fabricates a fighter of type " . $this->name . ")\n";
        else {
            echo "(Factory hasn't absorbed any fighter of type " . $this->name . ")\n";
            return NULL;
        }
        return self::_getClass($rf);
    }
}