<?php 

abstract class Fighter {
    private $_name;

    public function __construct($param)
    {
        if ($param)
            $this->_name = $param;
        else
            $this->_name = NULL;
    }

    public function __toString()
    {
        if ($this->_name)
            return $this->_name;
    }

    abstract public function fight($t);
}