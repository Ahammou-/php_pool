<?php
require_once 'Color.class.php';

class Color {
    public $red;
    public $green;
    public $blue;
    public static $verbose = false;
    /**
     * ==================================================== CONSTRUCTOR
     */
    public function __construct(array $kwargs) {
        if (array_key_exists('rgb',$kwargs)) {
            // B = C % 256
            // G = ((C-B)/256) % 256
            // R = ((C-B)/256**2) - G/256 
            $this->blue = (int)$kwargs['rgb'] % 256;
            $this->green = (((int)$kwargs['rgb'] - $this->blue) / 256) % 256;
            $this->red = ((int)$kwargs['rgb'] / pow(256,2)) - ($this->green / 256);
        } elseif (
            isset($kwargs['red']) && 
            isset($kwargs['green']) && 
            isset($kwargs['blue'])) {
                $this->red = (int)$kwargs['red'];
                $this->green = (int)$kwargs['green'];
                $this->blue = (int)$kwargs['blue'];
        }
        if (self::$verbose) {
            echo $this . "constructed.\n";
        }
    }
    /**
     * ==================================================== DESTRUCTOR
     */
    public function __destruct() {
        if (self::$verbose) {
            echo $this . "destructed.\n";
        }
    }

    /**
     * ==================================================== TO_STRING
     */
    public function __toString() {
        //return "Color( red: " .$this->red.", green: ".$this->green.", blue: ".$this->blue.") ";
        return sprintf("Color( red: %3d, green: %3d, blue: %3d ) ",$this->red,$this->green,$this->blue);
    }

    /**
     * ==================================================== DOCUMENTATION
     */
    public static function doc() {
        if ($file = file_get_contents('./Color.doc.txt'))
            echo "\n$file\n";
        else 
            echo "Error: .doc file doesn't exist.\n";
        if (self::$verbose) {
            echo $this;
        }
    }

    /**
     * ==================================================== ADD COLOR
     */
    public function add(Color $col) {
         
        $newColor = new Color([
            'red' => $this->red + $col->red,
            'green' => $this->green + $col->green,
            'blue' => $this->blue + $col->blue
         ]);
        return $newColor;
    }
   
    /**
     * ==================================================== SUBSTRACT COLOR
     */
    public function sub(Color $col) {
        $newColor = new Color([
            'red' => $this->red - $col->red,
            'green' => $this->green - $col->green,
            'blue' => $this->blue - $col->blue
        ]);
        return $newColor;
    }
    
    /**
     * ==================================================== MULTIPLY COLOR
     */
    public function mult($f) {
        $newColor = new Color([
            'red' => $this->red * $f,
            'green' => $this->green * $f,
            'blue' => $this->blue * $f
        ]);
        return $newColor;
    }

}