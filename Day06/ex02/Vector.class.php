<?php
	require_once 'Color.class.php';
	require_once 'Vertex.class.php';

	class Vector {
		private $_x;
		private $_y;
		private $_z;
		private $_w = 0.0;
		public static $verbose = false;

		/**
		 * ==================================================== CONSTRUCTOR
		 */
		public function __construct(array $vect) {
			if (array_key_exists('orig', $vect)) {
				$orig = new Vertex([
					'x' => $vect['orig']->getX(),
					'y' => $vect['orig']->getY(),
					'z' => $vect['orig']->getZ()
					]);
			} else if (!isset($orig)) {
				$orig = new Vertex(['x' => 0.0, 'y' => 0.0, 'z' => 0.0 , 'w' => 1.0]);
			}
			if (isset($vect['dest'])) {
				$this->_x = $vect['dest']->getX() - $orig->getX();
				$this->_y = $vect['dest']->getY() - $orig->getY();
				$this->_z = $vect['dest']->getZ() - $orig->getZ();
			}
			if (self::$verbose) {
				printf($this . " constructed\n");
			}
		}

		/**
		 * ==================================================== DESTRUCTOR
		 */
		public function __destruct() {
			if (self::$verbose) {
				printf($this . " destructed\n");
			}
		}

		/**
		 * ==================================================== TO_STRING
		 */
		public function __tostring() {
			return ($ret = sprintf(
				"Vector( x:%.2f, y:%.2f, z:%.2f, w:%.2f ) ",
				$this->_x, $this->_y, $this->_z, $this->_w) );
		}

		/**
		 * ==================================================== DOCUMENTATION
		 */
		public function doc() {
			if ($str = file_get_contents('Vector.doc.txt')) {
				echo "\n$str\n";
			}
			else {
				echo "Error: .doc file doesn't exist.\n";
			}
		}

		/**
		 * ==================================================== MAGNITUDE
		 */
		public function magnitude() {
			$magn = (float)sqrt(
				($this->_x - $orig->x) ** 2 +
				($this->_y - $orig->y) ** 2 +
				($this->_z - $orig->z) ** 2
			);
			if ($magn == 1) {
				return ("norm");
			} else {
				return ($magn);
			}
		}

		/**
		 * ==================================================== NORMALIZE
		 */
		public function normalize() {
			$len = $this->magnitude();
			if ($len == 1) {
				return clone $this;
			}
			$norm = new Vector(array('dest' => new Vertex(array(
				'x' => $this->_x / $len,
				'y' => $this->_y / $len,
				'z' => $this->_z / $len
			))));
			return ($norm);
		}

		/**
		 * ==================================================== ADD VECTOR
		 */
		public function add(Vector $rhs) {
			$add = new Vector(array('dest' => new Vertex(array(
				'x' => $this->_x + $rhs->_x,
				'y' => $this->_y + $rhs->_y,
				'z' => $this->_z + $rhs->_z
			))));
			return ($add);
		}

		/**
		 * ==================================================== SUBSTRACT VECTOR
		 */
		public function sub(Vector $rhs) {
			$sub = new Vector(array('dest' => new Vertex(array(
				'x' => $this->_x - $rhs->_x,
				'y' => $this->_y - $rhs->_y,
				'z' => $this->_z - $rhs->_z
			))));
			return ($sub);
		}

		/**
		 * ==================================================== VECTOR OPPOSITE
		 */
		public function opposite() {
			$opp = new Vector(array('dest' => new Vertex(array(
				'x' => $this->_x * (-1),
				'y' => $this->_y * (-1),
				'z' => $this->_z * (-1)
			))));
			return ($opp);
		}

		/**
		 * ==================================================== SCALAR PRODUCT
		 */
		public function scalarProduct($k) {
			$scl = new Vector(array('dest' => new Vertex(array(
				'x' => $this->_x * $k,
				'y' => $this->_y * $k,
				'z' => $this->_z * $k
			))));
			return ($scl);
		}

		/**
		 * ==================================================== DOT PRODUCT
		 */
		public function dotProduct(Vector $rhs) {
			$dot = (float)(
				$this->_x * $rhs->_x +
				$this->_y * $rhs->_y +
				$this->_z * $rhs->_z
			);
			return ($dot);
		}

		/**
		 * ==================================================== CROSS PRODUCT
		 */
		public function crossProduct(Vector $rhs) {
			$cross = new Vector(array('dest' => new Vertex(array(
				'x' => $this->_y * $rhs->_z - $this->_z * $rhs->_y,
				'y' => $this->_z * $rhs->_x - $this->_x * $rhs->_z,
				'z' => $this->_x * $rhs->_y - $this->_y * $rhs->_x
			))));
			return ($cross);
		}

		/**
		 * ==================================================== COSINUS
		 */
		public function cos(Vector $rhs) {
			if ($this->magnitude() == "norm"|| $rhs->magnitude() == "norm") {
					return (0);
			} else {
				$multilen = $this->magnitude() * $rhs->magnitude();
				return ($this->dotProduct($rhs) / $multilen);
			}
		}
		/**
		 * ==================================================== GETTERS
		 */
		private function getX() {
			return $this->_x;
		}
		private function getY() {
			return $this->_y;
		}
		private function getZ() {
			return $this->_z;
		}
		private function getW() {
			return $this->_w;
		}
	}