<?php
require_once "Color.class.php";
require_once "Vertex.class.php";
require_once "Vector.class.php";

class Matrix {
    const IDENTITY = [
        ['x'] => ['vtcX' => 1, 'vtcY' => 0, 'vtcZ' => 0,'vtxO' => 0],
        ['y'] => ['vtcX' => 1, 'vtcY' => 0, 'vtcZ' => 0,'vtxO' => 0],
        ['z'] => ['vtcX' => 1, 'vtcY' => 0, 'vtcZ' => 0,'vtxO' => 0]
    ];
    const SCALE = 2;
    const RX = 12;
    const RY = 11;
    const RZ = 5;
    const TRANSLATION = 1;
    const PROJECTION = 3;

    private $_preset;
    private $_scale;
    private $_angle;
    private $_vtc;
    private $_fov;
    private $_ratio;
    private $_matrix;
    public static $verbose = false;

    /**
     * ==================================================== CONSTRUCTOR
     */
    public function __construct(array $kwargs = NULL) {
       if
    }

    /**
     * ==================================================== DESTRUCTOR
     */
    public function __destruct() {
        if (self::$verbose) {
            echo $this . "destructed\n";
        }
    }

    /**
     * ==================================================== DOCUMENTATION
     */
    public static function doc() {
        if ($file = file_get_contents('./Matrix.doc.txt'))
            echo "$file" . "\n";
        else 
            echo "Error: .doc file doesn't exist.\n";
        if (self::$verbose) {
            echo $this;
        }
    }

    /**
     * ==================================================== CHOOSE FUNCTION FROM PRESET
     */
    public function choosePreset() {
        swhitch($this->_preset) {
            case (self::IDENTITY):
                $this->identity();
                break;
        }
    }

    /**
     * ==================================================== GET ARGUMENTS
     */
    public function getArgs(array $kwargs) {
        switch (array_key_exists($key, $kwargs)) {
            case 'preset':
                $this->_preset = $kwargs['preset'];
                break;
            case 'scale':
                $this->_scale = $kwargs['scale'];
                break;
            case 'angle':
                $this->_angle = $kwargs['angle'];
            case 'vtc':
                $this->_vtc = $kwargs['vtc'];
            case 'fov':
                $this->_fov = $kwargs['fov'];
            case 'ratio':
                $this->_ratio = $kwargs['ratio'];            
        }
    }




}