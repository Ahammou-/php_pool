<?php

class Vertex {
    private $_w = 1.0;
    private $_x;
    private $_y;
    private $_z;
    private $_color; 
    public static $verbose = false;

    /**
     * ==================================================== CONSTRUCTOR
     */
    public function __construct(array $kwargs) {
        if (array_key_exists('w', $kwargs)) {
            $this->_w = $kwargs['w'];
        }
        if (array_key_exists('color', $kwargs)) {
            $this->_color = $kwargs['color'];
        } else {
            $this->_color = new Color([
                'red' => 255,
                'green' => 255,
                'blue' => 255
            ]);
        }
        if (
            isset($kwargs['x']) &&
            isset($kwargs['y']) &&
            isset($kwargs['z'])) {
                $this->_x = $kwargs['x'];
                $this->_y = $kwargs['y'];
                $this->_z = $kwargs['z'];
        }
        if (self::$verbose) 
            echo $this . "constructed\n";
    }
    /**
     * ==================================================== DESTRUCTOR
     */
    public function __destruct() {
        if (self::$verbose) {
            echo $this . "destructed\n";
        }
    }
        /**
     * ==================================================== GETTERS
     */
    public function getW()
    {
        return $this->_w;
    }
    public function getX()
    {
        return $this->_x;
    }
    public function getY()
    {
        return $this->_y;
    }
    public function getZ()
    {
        return $this->_z;
    }
    public function getColor()
    {
        return $this->_color;
    }

    /**
     * ==================================================== SETTERS
     */
    public function setW($w)
    {
        $this->_w = $w;
    }
    public function setX($x)
    {
        $this->_x = $x;
    }
    public function setY($y)
    {
        $this->_y = $y;
    }
    public function setZ($z)
    {
        $this->_z = $z;
    }
    public function setColor($color)
    {
        $this->_color = $color;
    }

    /**
     * ==================================================== TO_STRING
     */
    public function __toString() {
        //Vertex( x: 0.00, y: 0.00, z:0.00, w:1.00, Color( red: 255, green: 255, blue: 255 ) ) 
       
        return sprintf("Vertex( x: %.2f, y: %.2f, z: %.2f, w: %.2f, %s) ", 
        $this->_x, $this->_y, $this->_z, $this->_w, $this->_color);
    }

    /**
     * ==================================================== DOCUMENTATION
     */
    public static function doc() {
        if ($file = file_get_contents('./Vertex.doc.txt'))
            echo "$file" . "\n";
        else 
            echo "Error: .doc file doesn't exist.\n";
        if (self::$verbose) {
            echo $this;
        }
    }
}