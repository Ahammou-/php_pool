<?php require_once "header.php";
session_start();
$sql = "SELECT * FROM products";
$res = mysqli_query($conn, $sql);

function chkBasket ($id, $ret = NULL)
{
    foreach($_SESSION['basket'] as $value)
    {
        if ($value['id'] === $id){
            if ($ret)
                return ($value['quantity']);
            else
                return TRUE;
            }
    }
    return FALSE;
}

if ($_POST['button'] === "Add to cart"){ 
 //   $_SESSION['basket'] = '';
    if (!chkBasket ($_POST['id']))
        $_SESSION['basket'][] = ["quantity" => $_POST['quantity'], "id" => $_POST['id']];
    else
        foreach ($_SESSION['basket'] as $key => $value)
            if ($value['id'] == $_POST['id'])
                $_SESSION['basket'][$key]["quantity"] = $_POST['quantity'];

    //unset($_SESSION['basket']);
    header("Location: index.php");
   // var_dump($_SESSION);
}
//var_dump($_SESSION);
?>

<style>
    body{
        background-color:beige;
    }
    .artiBox{
        width:100px;
        display:inline-block;
        margin: 20px;
        border-style:solid;
        border-width:2px;
        background-color:lightblue;
        border-radius:6px;
    }
    .artiBox img{
        width:80px;
        margin: 5px;
    }
    input{
        margin: 5px;
        width:25px;
    }
    .artiCat{
        font-style:italic;
        margin: 20px;
    }
    .artiPrice{
        margin: 5px;
    }
    .artiDescr{
        margin: 5px;
    }
    .artiQuant{
        margin: 5px;s
    }
</style>
<?php
foreach ($res as $k => $v) {
?>
    <div class="artiBox">
        <form name="<?=$v['name']?>Form" action="index.php" method="POST">
            <img src="<?=$v['image']?>" title="<?=$v['name']?>"\>
            <h3><?=$v['name']?></h3>
            <p class="artiCat"><?=$v['category']?></p>
            <p class="artiQuant">Available quantity: <?=$v['quantity']?></p>
            <p class="artiDescr"><?=$v['description']?></p>
            <input name="id" type="hidden" value="<?=$v['id']?>">
            <div class="artiPrice"><?=$v['price']?> €</div><input type="text" name="quantity" value="<?= chkBasket ($v['id'], TRUE) != NULL ?  chkBasket ($v['id'], TRUE) : ''?>"> <input type="submit" name="button" value="Add to cart">
        </form>
    </div>
<?php
}

require_once "footer.php"; ?>