<?php

require_once("header.php");
session_start();
$sql = "SELECT * FROM products";
$res = mysqli_query($conn, $sql);

?>
<h2>VOTRE PANIER</h2>
<table class="list">
    <tr>
        <th>Nom : </th>
        <th>Quantité : </th>
        <th>Prix total : </th>
    </tr>
    <?php
    foreach ($_SESSION['basket'] as $key => $value) {
        foreach ($res as $k2 => $v2) {
            if ($v2['id'] === $value['id']) {
                ?>
                <tr>
                    <td><?= $v2['name']; ?></td>
                    <td><?= $value['quantity']; ?></td>
                    <td><?= $value['quantity'] * $v2['price']; ?></td>
                </tr>
                <?php
            }
        }
    }
    ?>
</table>
<?php

require_once("footer.php");

?>