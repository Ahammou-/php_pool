<?php

$sql = "SELECT * FROM categories";
$res = mysqli_query($conn, $sql);

?>
<h2>Liste des catégories</h2>
    <a class="opt" href="./add.php?page=category">Ajouter une catégorie</a>
    <table class="list">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Choose</th>
        </tr>
        <?php
        foreach ($res as $k => $v) {
        ?>
        <tr>
            <td><?= $v['id']; ?></td>        
            <td><?= $v['name']; ?></td>
            <td>
            <a class="opt" href="./edit.php?id=<?= $v['id'];?>&page=category">modifier</a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>