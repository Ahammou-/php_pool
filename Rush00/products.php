<?php

$sql = "SELECT * FROM products";
$res = mysqli_query($conn, $sql);

?>
<h2>Liste des articles</h2>
    <a class="opt" href="./add.php?page=products">Ajouter un article</a>
    <table class="list">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Description</th>
            <th>Stock</th>
            <th>Prix/unitaire</th>
            <th>Catégorie</th>
            <th>Image</th>
            <th>Choose</th>
        </tr>
        <?php
        foreach ($res as $k => $v) {
        ?>
        <tr>
            <td><?= $v['id']; ?></td>        
            <td><?= $v['name']; ?></td>
            <td><?= $v['description']; ?></td>
            <td><?= $v['quantity']; ?></td>
            <td><?= $v['price']; ?></td>
            <td><?= $v['category']; ?></td>
            <td><?= $v['image']; ?></td>
            <td>
            <a class="opt" href="./edit.php?id=<?= $v['id'];?>&page=products">modifier</a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>