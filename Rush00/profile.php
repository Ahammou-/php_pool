<?php
session_start();
require_once "header.php";

?>
<h1>VOTRE PROFIL</h1>

<form action="php/editUser.php" class="form" method="POST">
    <table>
        <tr>
            <td><label for="firstname">Prénom : </label></td>
            <td><input type="text" name="firstname" value="<?= $_SESSION['firstname']; ?>"></td>
        </tr>
        <tr>
            <td><label for="lastname">Nom : </label></td>
            <td><input type="text" name="lastname" value="<?= $_SESSION['lastname']; ?>"></td>
        </tr>
        <tr>
            <td><label for="email">Email : </label></td>
            <td><input type="email" name="email" value="<?= $_SESSION['email']; ?>"></td>
        </tr>
        <tr>
            <td><label for="login">login : </label></td>
            <td><input type="text" name="login" value="<?= $_SESSION['login']; ?>"></td>
        </tr>
        <tr>
            <td><label for="passwd">Mot de passe : </label></td>
            <td><input type="password" name="passwd" value=""></td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td><input type="submit" name="submit" value="modifier"> | <input type="submit" name="submit" value="supprimer"></td>
        </tr>
    </table>
    <section>
        <?php include "user.php"; ?>
    </section>
    <section>
        <?php include "category.php"; ?>
    </section>
    <section>
        <?php include "products.php"; ?>
    </section>
</form>

<?php require_once "footer.php"; ?>