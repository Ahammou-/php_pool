<?php

$sql = "SELECT * FROM users";
$res = mysqli_query($conn, $sql);

?>
<h2>Liste des users</h2>
    <a class="opt" href="./add.php?page=user">Ajouter un user</a>
    <table class="list">
        <tr>
            <th>ID</th>
            <th>Login</th>
            <th>Mot de passe</th>
            <th>Prénom</th>
            <th>Nom</th>
            <th>e-mail</th>
            <td>Rights</td>
            <th>Choose</th>
        </tr>
        <?php
        foreach ($res as $k => $v) {
        ?>
        <tr>
            <td><?= $v['id']; ?></td>        
            <td><?= $v['login']; ?></td>
            <td></td>
            <td><?= $v['firstname']; ?></td>
            <td><?= $v['lastname']; ?></td>
            <td><?= $v['email']; ?></td>
            <td><?= $v['rights']; ?></td>
            <td>
                <a class="opt" href="./edit.php?id=<?= $v['id'];?>&page=user">modifier</a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
