<?php
require_once "connectDB.php";
$conn = connectDB();
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
    <div class="nav c-purple">
        <ul>
            <li><a href="index.php">home</a></li>
            <li><a href="register.php">inscription</a></li>
            <li><a href="login.php">connexion</a></li>
            <li><a href="profile.php">profil</a></li>
            <li><a href="logout.php">logout</a></li>
            <li><a href="basket.php">panier</a></li>
        </ul>
    </div>
    