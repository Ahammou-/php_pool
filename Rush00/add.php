<?php
session_start();
require_once "header.php";

$sql = "SELECT * FROM users WHERE id='".$_GET['id']."'";
$res = mysqli_query($conn, $sql);
$array = mysqli_fetch_assoc($res);
$array['logged'] = true;

$sqlCat = "SELECT * FROM categories WHERE id='".$_GET['id']."'";
$res = mysqli_query($conn, $sqlCat);
$cat = mysqli_fetch_assoc($res);

$sqlArt = "SELECT * FROM products WHERE id='".$_GET['id']."'";
$res = mysqli_query($conn, $sqlArt);
$art = mysqli_fetch_assoc($res);

if ($_GET['page'] == "user") {
?>
<h1>AJOUTER UTILISATEUR</h1>

<form action="php/addUser.php" class="form" method="POST">
    <table>
        <tr>
            <td><label for="firstname">Prénom : </label></td>
            <td><input type="text" name="firstname" value="<?= $array['firstname']; ?>"></td>
        </tr>
        <tr>
            <td><label for="lastname">Nom : </label></td>
            <td><input type="text" name="lastname" value="<?= $array['lastname']; ?>"></td>
        </tr>
        <tr>
            <td><label for="email">Email : </label></td>
            <td><input type="email" name="email" value="<?= $array['email']; ?>"></td>
        </tr>
        <tr>
            <td><label for="login">login : </label></td>
            <td><input type="text" name="login" value="<?= $array['login']; ?>"></td>
        </tr>
        <tr>
            <td><label for="passwd">Mot de passe : </label></td>
            <td><input type="password" name="passwd" value=""></td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td><input type="submit" name="submit" value="creer"></td>
        </tr>
    </table>
    
</form>
<?php
}elseif ($_GET['page'] == "category") {
?>

<h1>AJOUTER LES CATEGORIES</h1>

<form action="php/addCat.php" class="form" method="POST">
    <table>
        <tr>
            <td><input type="hidden" name="id" value="<?= $_GET['id']; ?>"></td>
        </tr>
        <tr>
            <td><label for="name">Nom : </label></td>
            <td><input type="text" name="name" value="<?= $cat['name']; ?>"></td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td><input type="submit" name="submit" value="ajouter"></td>
        </tr>
    </table>
    
</form>

<?php
}else {
?>
<h1>AJOUTER LES ARTICLES</h1>

<form action="php/addArt.php" class="form" method="POST">
    <table>
        <tr>
            <td><input type="hidden" name="id" value="<?= $_GET['id']; ?>"></td>
        </tr>
        <tr>
            <td><label for="name">Nom : </label></td>
            <td><input type="text" name="name" value="<?= $art['name']; ?>"></td>
        </tr>
        <tr>
            <td><label for="dscr">Description : </label></td>
            <td><input type="text" name="dscr" value="<?= $art['description']; ?>"></td>
        </tr>
        <tr>
            <td><label for="qte">Stock : </label></td>
            <td><input type="text" name="qte" value="<?= $art['quantity']; ?>"></td>
        </tr>
        <tr>
            <td><label for="prix">Prix/unitaire : </label></td>
            <td><input type="text" name="prix" value="<?= $art['price']; ?>"></td>
        </tr>
        <tr>
            <td><label for="cat">Catégorie : </label></td>
            <td><input type="text" name="cat" value="<?= $art['category']; ?>"></td>
        </tr>
        <tr>
            <td><label for="img">Image : </label></td>
            <td><input type="text" name="img" value="<?= $art['image']; ?>"></td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td><input type="submit" name="submit" value="ajouter"></td>
        </tr>
    </table>
    
</form>
<?php
}
require_once "footer.php";
?>